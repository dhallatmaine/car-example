import java.util.List;

public class Car {

  private Person driver;
  private List<Person> passengers;
  private String model;
  private String color;
  private int year;

  public Person getDriver() { return driver; }
  public void setDriver(Person driver) { this.driver = driver; }

  public List<Person> getPassengers() { return passengers; }
  public void setPassengers(List<Person> passengers) { this.passengers = passengers; }

  public String getModel() { return model; }
  public void setModel(String model) { this.model = model; }

  public String getColor() { return color; }
  public void setColor(String color) { this.color = color; }

  public int getYear() { return year; }
  public void setYear(int year) { this.year = year; }

}