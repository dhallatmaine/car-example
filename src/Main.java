import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    Person fred = new Person();
    fred.setName("Fred");
    Person tina = new Person();
    tina.setName("Tina");

    List<Person> mazdaPassengers = new ArrayList<Person>();
    mazdaPassengers.add(fred);
    mazdaPassengers.add(tina);

    Car mazda = new Car();
    mazda.setColor("silver");
    mazda.setModel("mazda");
    mazda.setYear(2014);
    mazda.setDriver(fred);
    mazda.setPassengers(mazdaPassengers);
  }

}